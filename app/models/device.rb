class Device < ActiveRecord::Base
  attr_accessible :computer_name, :key, :brand, :device_type, :assignment, :location, :manufacturer, :change_location, :date_in_service, :asset_tag, :serial_number, :disposal_date, :mac1, :mac2, :cpu, :ghz, :core, :hdd, :screen, :change
  attr_accessible :ethernet_mac_address, :wireless_mac_address, :os, :printer, :projector, :sb, :age, :notes, :memory, :lease_id, :lease_term

  validates :asset_tag, uniqueness: true, allow_blank: true
  validates :serial_number, uniqueness: true, allow_blank: true
  
  def self.import(file)
     spreadsheet = Import::Uploader.open_spreadsheet(file)
     spreadsheet.default_sheet = "Devices"
     Processheet.process_spreadsheet(spreadsheet, self)
   end
   
   def self.process_result(row)
    device = find_by_computer_name(row["computer_name"]) || new
    device.attributes = row.to_hash.slice(*accessible_attributes)
    device.save!
  end
  
  def self.search(search)
    if search
      where('computer_name ILIKE ? or device_type ILIKE ? or assignment ILIKE ? or location ILIKE ? or asset_tag ILIKE ? or serial_number ILIKE ? or change ILIKE ?', "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
    else
      scoped
    end
  end
end
