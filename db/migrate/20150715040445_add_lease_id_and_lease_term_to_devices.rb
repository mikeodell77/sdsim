class AddLeaseIdAndLeaseTermToDevices < ActiveRecord::Migration
  def change
    add_column :devices, :lease_id, :string
    add_column :devices, :lease_term, :integer
  end
end
